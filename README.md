# Tapitoo Infrastructure #

Using it with `docker-composer.yml` it will start 3 containers as follows:

* tomcat8
* mysql-server
* phpmyadmin

Please look at the `docker-compose.yml` file for details about each container that is started.

**Note:** On `phpmyadmin` is where nginx is installed at *1.7.12* version. See details [here](https://github.com/corbinu/docker-phpmyadmin).

## Useful commands: ##
* Start services: `docker-compose -f docker-compose.yml up -d`
* Stop services:  `docker-compose -f docker-compose.yml stop`
* Remove containers: `docker-compose -f docker-compose.yml rm`
* SSH to a container: `docker exec -it <container id|name> bash`

## Known issues ##
When first starting the `phpmyadmin` container you need to create the *phpmyadmin* table. There will be a warning prompting you to do it. Still working on solving it.

**Update** This seems to be an issue only on Windows :(